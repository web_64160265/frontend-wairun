import type Checkmaterial from "@/types/CheckMaterial";
import http from "./axios";
function getCheckmaterials(params: any) {
  return http.get("/checkmat", { params: params });
}

function saveCheckMaterials(checkmaterial: {
  employeeId: number;
  checkmatdetails: {
    materialId: number;
    qty_remain: number;
    qty_expire: number;
  }[];
}) {
  return http.post("/checkmat/", checkmaterial);
}

function updateCheckmaterials(id: number, checkmaterial: Checkmaterial) {
  return http.patch(`/checkmat/${id}`, checkmaterial);
}
function deleteCheckmaterials(id: number) {
  return http.delete(`/checkmat/${id}`);
}
export default {
  getCheckmaterials,

  updateCheckmaterials,
  deleteCheckmaterials,
  saveCheckMaterials,
};
