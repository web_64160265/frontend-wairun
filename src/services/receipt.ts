import type Receipt from "@/types/Receipt";
import http from "./axios";
function getReceipts() {
  return http.get("/receipts");
}

function saveReceipt(receipt: {
  customerId?: number;
  storeId: number;
  employeeId: number;
  receiptdetails: { productId: number; amount: number }[];
  received: number;
  discount: number;
  payment: string;
}) {
  return http.post("/receipts", receipt);
}

function updateReceipt(id: number, receipt: Receipt) {
  return http.patch(`/receipts/${id}`, receipt);
}

function deleteReceipt(id: number) {
  return http.delete(`/receipts/${id}`);
}

export default { getReceipts, saveReceipt, updateReceipt, deleteReceipt };
