import http from "./axios";
function getCheckinouts(params: any) {
  return http.get("/checkinout", { params: params });
}

function saveCheckinouts(checkinout: { employeeId: number }) {
  return http.post("/checkinout", checkinout);
}
function updateCheckinouts(id: number) {
  return http.patch(`/checkinout/${id}`);
}
function daleteCheckinouts(id: number) {
  return http.delete(`/checkinout/${id}`);
}
export default {
  getCheckinouts,
  saveCheckinouts,
  updateCheckinouts,
  daleteCheckinouts,
};
