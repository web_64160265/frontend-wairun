import type Material from "@/types/Material";
import http from "./axios";
function getMaterials(params: any) {
  return http.get("/material", { params: params });
}
function saveMaterials(materials: {
  name: string;
  min_quantity: number;
  quantity: number;
  unit: string;
  price_per_unit: number;
}) {
  return http.post("/material", materials);
}
function updateMaterials(id: number, materials: Material) {
  return http.patch(`/material/${id}`, materials);
}
function deleteMaterials(id: number) {
  return http.delete(`/material/${id}`);
}
export default {
  getMaterials,
  saveMaterials,
  updateMaterials,
  deleteMaterials,
};
