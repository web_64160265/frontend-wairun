import type Customer from "@/types/Customer";
import http from "./axios";
function getCustomers(params: any) {
  return http.get("/customers", { params: params });
}

function saveCustomer(customer: Customer) {
  return http.post("/customers", customer);
}

function updateCustomer(id: number, customer: Customer) {
  return http.patch(`/customers/${id}`, customer);
}

function deleteCustomer(id: number) {
  return http.delete(`/customers/${id}`);
}

export default { getCustomers, saveCustomer, updateCustomer, deleteCustomer };
