import type SummarySalary from "@/types/Summarysalary";
import http from "./axios";
function getSummarysalary() {
  return http.get("/summarysalarys");
}
function saveSummarysalary(saveSummarysalary: SummarySalary) {
  return http.post("/summarysalarys", saveSummarysalary);
}
function updateSummarysalary(id: number, summarysalary: SummarySalary) {
  return http.patch(`/summarysalarys/${id}`, summarysalary);
}
function daleteSummarysalary(id: number) {
  return http.delete(`/summarysalarys/${id}`);
}
export default {
  getSummarysalary,
  saveSummarysalary,
  updateSummarysalary,
  daleteSummarysalary,
};
