import http from "./axios";

function getReportCustomer() {
  return http.get("/reports/customers/getCustomer");
}
function getCustomerByPoint() {
  return http.get("/reports/customer/bypoint");
}
function getCustomer_info() {
  return http.get("/reports/customers/getCustomer_info");
}
function getCustomeView() {
  return http.get("/reports/view/customer");
}
function getTotalsalebyday() {
  return http.get("/reports/view/getTotalsalebyday");
}
function getBestOrderChart() {
  return http.get("/reports/recieptdetail/bestseller/chart");
}

function getWorstOrderChart() {
  return http.get("/reports/recieptdetail/worstseller/chart");
}
function getDayofWeektotalamount() {
  return http.get("/reports/recieptdetail/dayofweektotalamount/chart");
}
function getDayofweekTotalsales() {
  return http.get("/reports/recieptdetail/dayofweektotalsale/chart");
}
console.log(getTotalsalebyday());
export default {
  getReportCustomer,
  getCustomer_info,
  getCustomeView,
  getCustomerByPoint,
  getTotalsalebyday,
  getWorstOrderChart,
  getBestOrderChart,
  getDayofWeektotalamount,
  getDayofweekTotalsales,
};
