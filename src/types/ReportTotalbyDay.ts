export default interface ReportBestSaller {
  id?: number;
  SumTotal: number;
  SumQuantity: number;
}
