export default interface ReportSeasontotal {
  id?: number;
  day_of_week: string;
  total_sales: number;
}
