import type Customer from "./Customer";
import type Employee from "./Employee";
import type Receiptdetail from "./Receiptdetail";
import type Store from "./Store";

export default interface Receipt {
  id?: number;
  date: Date;
  discount: number;
  total: number;
  received: number;
  change: number;
  payment: string;
  amount: number;
  updatedDate: Date;
  deletedDate: Date;
  receiptdetails: Receiptdetail[];
  customer?: Customer;
  store: Store;
  employee: Employee;
  show?: boolean;
}
