export default interface ReportTotalbyDay {
  sales_day: string;
  total_products_sold: number;
}
