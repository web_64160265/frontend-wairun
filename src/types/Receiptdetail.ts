import type Product from "./Product";
import type Receipt from "./Receipt";

export default interface Receiptdetail {
  id?: number;
  name?: string;
  price?: number;
  amount?: number;
  total?: number;
  receipts?: Receipt;
  product?: Product; // Product Id
  createdDate?: Date;
  updatedDate?: Date;
  deletedDate?: Date;
}
