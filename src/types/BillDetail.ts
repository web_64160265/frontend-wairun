import type Bill from "./Bill";
import type Material from "./Material";

export default interface BillDetail {
  id?: number;

  name?: string;

  amount?: number;

  price?: number;

  total?: number;

  material?: Material;

  bills?: Bill;

  createdDate?: Date;

  updatedDate?: Date;

  deletedDate?: Date;
}
