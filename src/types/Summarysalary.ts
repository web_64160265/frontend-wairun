import type User from "./User";

export default interface SummarySalary {
  id?: number;

  date: Date;

  work_hour: number;

  salary: number;

  employee: User;

  createdAt?: Date;

  updatedAt?: Date;

  deletedAt?: Date;
}
