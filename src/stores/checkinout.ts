import { defineStore } from "pinia";
import { useLoadingStore } from "./loading";
import { useAuthStore } from "./auth";
import checkinoutService from "@/services/checkinout";
import { useMessageStore } from "./message";
// import { useMessageSuccessStore } from "./messageSuccess";
import { ref } from "vue";
import router from "@/router";
export const useCheckinoutStore = defineStore("Checkinout", () => {
  const loadingStore = useLoadingStore();
  const authStore = useAuthStore();
  const messageStore = useMessageStore();
  // const messageSuccessStore = useMessageSuccessStore();
  const checkinoutId = ref(0);
  async function openCheckin() {
    try {
      loadingStore.isLoading = true;
      const user: { id: number } = authStore.getUser();
      const checkinout = {
        employeeId: user.id,
      };
      const res = await checkinoutService.saveCheckinouts(checkinout);
      checkinoutId.value = res.data.id;
      messageStore.showInfo("Check In Successful");
      console.log("checkinoutId", checkinoutId);
      console.log("user", user);
      console.log("employeeId", checkinout);
      console.log("res", res);
    } catch (e) {
      console.log(e);
      messageStore.showError("Unable To Check In");
    }
    loadingStore.isLoading = false;
    router.replace("/empman");
  }

  async function openCheckout() {
    loadingStore.isLoading = true;
    try {
      const res = await checkinoutService.updateCheckinouts(checkinoutId.value);
      messageStore.showInfo("Check Out Successful");
      console.log(res.data);
      console.log(checkinoutId.value);
    } catch (e) {
      console.log("น้อง e", e);
      messageStore.showError("Unable To Check Out");
    }
    loadingStore.isLoading = false;
    router.replace("/empman");
  }
  return {
    openCheckin,
    openCheckout,
    checkinoutId,
  };
});
