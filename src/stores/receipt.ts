import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Receipt from "@/types/Receipt";
import receiptService from "@/services/receipt";
import reportService from "@/services/reports";
import customerService from "@/services/customer";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import Swal from "sweetalert2";
import type Product from "@/types/Product";
import { useAuthStore } from "./auth";
import { useCustomerStore } from "./customer";
import product from "@/services/product";
import type ReportTotalbyDay from "@/types/ReportTotalbyDay";

export const useReceiptStore = defineStore("Receipt", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const customerStore = useCustomerStore();
  const authStore = useAuthStore();
  const dialog = ref(false);
  const received = ref(0);
  const totalbyday = ref<ReportTotalbyDay[]>([]);
  const payment = ref("");
  const receipts = ref<Receipt[]>([]);
  const customerId = ref(0);
  // const editedReceipt = ref<Receipt>({});
  const receiptList = ref<
    { product: Product; amount: number; sum: number; price: number }[]
  >([]);
  async function getTotalsalebyday() {
    try {
      const res = await reportService.getTotalsalebyday();
      totalbyday.value = res.data;
      console.log("receiptsStore.totalbyday:", totalbyday.value);
    } catch (error) {
      console.error(error);
    }
  }
  function Cash() {
    return (payment.value = "Cash");
  }
  function Credit() {
    return (payment.value = "Credit");
  }
  function QRcode() {
    return (payment.value = "QRcode");
  }
  function clearReceipt() {
    receiptList.value = [];
  }
  function addProduct(item: Product) {
    for (let i = 0; i < receiptList.value.length; i++) {
      if (receiptList.value[i].product.id === item.id) {
        receiptList.value[i].amount++;
        receiptList.value[i].sum = receiptList.value[i].amount * item.price;
        return;
      }
    }

    receiptList.value.push({
      product: item,
      amount: 1,
      sum: 1 * item.price,
      price: item.price,
    });
  }
  function deleteProduct(index: number) {
    receiptList.value.splice(index, 1);
  }
  function increaseProduct(i: number) {
    receiptList.value[i].amount++;
    receiptList.value[i].sum =
      receiptList.value[i].amount * receiptList.value[i].price;
    discountCalculate();
  }
  function decreaseProduct(i: number) {
    receiptList.value[i].amount--;
    receiptList.value[i].sum =
      receiptList.value[i].amount * receiptList.value[i].price;
    discountCalculate();
    if (receiptList.value[i].amount == 0) {
      deleteProduct(i);
    }
  }
  const discount = ref(0);
  function discountCalculate() {
    if (customerStore.status == true) {
      if (customerStore.usingCus.point >= 10) {
        discount.value = sumPrice.value * 0.2;
      }
    } else {
      discount.value = 0;
    }
  }
  const sumAmount = computed(() => {
    let sum = 0;
    for (let i = 0; i < receiptList.value.length; i++) {
      sum = sum + receiptList.value[i].amount;
    }
    return sum;
  });
  const sumPrice = computed(() => {
    let sum = 0;
    for (let i = 0; i < receiptList.value.length; i++) {
      sum = sum + receiptList.value[i].sum;
    }
    return sum;
  });

  // watch(dialog, (newDialog, oldDialog) => {
  //   console.log(newDialog);
  //   if (!newDialog) {
  //     editedReceipt.value = {};
  //   }
  // });
  async function getReceipts() {
    loadingStore.isLoading = true;
    try {
      const res = await receiptService.getReceipts();
      receipts.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Receipt ได้");
    }
    loadingStore.isLoading = false;
  }

  async function openReceipt() {
    discountCalculate();
    loadingStore.isLoading = true;
    const user: { id: number } = authStore.getUser();
    const receiptDetails = receiptList.value.map(
      (item) =>
        <{ productId: number; amount: number }>{
          productId: item.product.id,
          amount: item.amount,
        }
    );
    const receipt = {
      customerId: customerStore.usingCus.id,
      storeId: 1,
      employeeId: user.id,
      receiptdetails: receiptDetails,
      received: received.value,
      discount: discount.value,
      payment: payment.value,
    };
    console.log(receipt);
    try {
      const res = await receiptService.saveReceipt(receipt);
      dialog.value = false;
      customerStore.POSpointMember += sumAmount.value;
      const Customer = {
        name: customerStore.usingCus.name,
        tel: customerStore.usingCus.tel,
        point: customerStore.POSpointMember,
      };
      if (customerStore.usingCus.id?.valueOf() != null) {
        const res2 = await customerService.updateCustomer(
          customerStore.usingCus.id?.valueOf(),
          Customer
        );
      }
      await customerStore.getCustomers();
      // await getReceipts();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Receipt ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  const receiptShow = ref(false);
  async function checkShowingReceipt() {
    if (receiptShow.value == false) {
      clearReceipt();
      customerStore.usingCus.id = 0;
      customerStore.usingCus.name = "";
      customerStore.usingCus.tel = "";
      customerStore.usingCus.point = 0;
      customerStore.usingCus.start_date = "";
      customerStore.status = false;
      received.value = 0;
      discount.value = 0;
      loadingStore.isLoading = false;
    }
  }
  // async function saveReceipt() {
  //   loadingStore.isLoading = true;
  //   try {
  //     if (editedReceipt.value.id) {
  //       const res = await receiptService.updateReceipt(
  //         editedReceipt.value.id,
  //         editedReceipt.value
  //       );
  //     } else {
  //       const res = await receiptService.saveReceipt(editedReceipt.value);
  //     }

  //     dialog.value = false;
  //     await getReceipts();
  //   } catch (e) {
  //     messageStore.showError("ไม่สามารถบันทึก Receipt ได้");
  //     console.log(e);
  //   }
  //   loadingStore.isLoading = false;
  // }

  async function deleteReceipt(id: number) {
    const confirmed = await Swal.fire({
      title: "Are you sure?",
      text: "You will not be able to recover this receipt!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    });

    if (confirmed.isConfirmed) {
      loadingStore.isLoading = true;
      try {
        const res = await receiptService.deleteReceipt(id);
        await getReceipts();
        messageStore.showConfirm("Receipt deleted successfully");
      } catch (e) {
        console.log(e);
        messageStore.showError("Failed to delete receipt");
      }
      loadingStore.isLoading = false;
    }
  }
  // function editReceipt(receipt: Receipt) {
  //   editedReceipt.value = JSON.parse(JSON.stringify(receipt));
  //   dialog.value = true;
  // }
  return {
    receipts,
    getReceipts,
    dialog,
    // editedReceipt,
    // saveReceipt,
    // editReceipt,
    deleteReceipt,
    addProduct,
    deleteProduct,
    sumAmount,
    sumPrice,
    openReceipt,
    receiptList,
    clearReceipt,
    received,
    payment,
    Cash,
    Credit,
    QRcode,
    receiptShow,
    checkShowingReceipt,
    increaseProduct,
    decreaseProduct,
    discount,
    discountCalculate,
    totalbyday,
    getTotalsalebyday,
  };
});
