import { defineStore } from "pinia";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type Checkmaterialdetail from "@/types/CheckMaterialDetail";
import { ref, watch } from "vue";
import checkmaterialdetailService from "@/services/checkmaterialdetail";
import { useMaterialStore } from "./material";
import { useCheckMaterialStore } from "./checkMaterial";

export const useCheckMaterialDetailStore = defineStore(
  "Checkmaterialdetail",
  () => {
    const loadingStore = useLoadingStore();
    const messageStore = useMessageStore();
    const materialStore = useMaterialStore();
    const usecheckMaterialStore = useCheckMaterialStore();
    const dialog = ref(false);
    const dialog2 = ref(false);
    const checkmaterialdetails = ref<Checkmaterialdetail[]>([]);
    const editedCheckmaterialDetail = ref<Checkmaterialdetail>({
      name: "",
      qty_last: 0,
      qty_remain: 0,
      qty_expire: 0,
      material: {
        name: "",
        min_quantity: 0,
        quantity: 0,
        qty_remain: 1,
        qty_expire: 1,
        unit: "",
        price_per_unit: 1,
      },
      checkmaterial: {
        employeeId: 1,
      },
    });

    watch(dialog, (newDialog, oldDialog) => {
      console.log(newDialog);
      if (!newDialog) {
        editedCheckmaterialDetail.value = {
          name: "",
          qty_last: 0,
          qty_remain: 0,
          qty_expire: 0,
          material: {
            name: "",
            min_quantity: 0,
            quantity: 0,
            qty_remain: 1,
            qty_expire: 1,
            unit: "",
            price_per_unit: 1,
          },
          checkmaterial: {
            employeeId: 1,
          },
        };
      }
    });
    watch(dialog2, (newDialog, oldDialog) => {
      console.log(newDialog);
      if (!newDialog) {
        editedCheckmaterialDetail.value = {
          name: "",
          qty_last: 0,
          qty_remain: 0,
          qty_expire: 0,
          material: {
            name: "",
            min_quantity: 0,
            quantity: 0,
            qty_remain: 1,
            qty_expire: 1,
            unit: "",
            price_per_unit: 1,
          },
          checkmaterial: {
            employeeId: 1,
          },
        };
      }
    });
    async function getCheckmaterialDetails() {
      loadingStore.isLoading = true;
      try {
        const res = await checkmaterialdetailService.getCheckmaterialdetails(
          {}
        );
        checkmaterialdetails.value = res.data;
      } catch (e) {
        console.log(e);
        messageStore.showError("ไม่สามารถดึงข้อมูล Check Material ได้");
      }
      loadingStore.isLoading = false;
    }

    function editCheckmaterialdetail(checkmaterialdetail: Checkmaterialdetail) {
      editedCheckmaterialDetail.value = JSON.parse(
        JSON.stringify(checkmaterialdetail)
      );
      dialog.value = true;
      dialog2.value = true;
    }

    return {
      editedCheckmaterialDetail,
      dialog,
      dialog2,
      getCheckmaterialDetails,
      editCheckmaterialdetail,
    };
  }
);
