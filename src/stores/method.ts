import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useMethod = defineStore("counter", () => {
  const order = ref(0);
  function increment() {
    order.value++;
  }

  return { order, increment };
});
