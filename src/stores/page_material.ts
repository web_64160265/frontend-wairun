import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Material from "@/types/Material";
import { useLoadingStore } from "./loading";
import materialService from "@/services/material";
import { useMessageStore } from "./message";
import Swal from "sweetalert2";

export const useMaterialPageStore = defineStore("materialPage", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const keyword = ref("");
  const materials = ref<Material[]>([]);

  const editedMaterialPage = ref<Material>({
    name: "",
    min_quantity: 0,
    quantity: 0,
    unit: "",
    price_per_unit: 0,
    qty_remain: 0,
    qty_expire: 0,
  });
  const lastPage = ref(0);
  watch(keyword, async (newKeyword, oldKeyword) => {
    await getMaterials();
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedMaterialPage.value = {
        name: "",

        min_quantity: 0,
        quantity: 0,
        unit: "",
        price_per_unit: 0,
        qty_remain: 0,
        qty_expire: 0,
      };
    }
  });
  async function getMaterials() {
    loadingStore.isLoading = true;
    try {
      console.log(materials.value);
      const res = await materialService.getMaterials({
        keyword: keyword.value,
      });
      materials.value = res.data.data;

      console.log(materials.value);
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Employee ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveMaterial() {
    loadingStore.isLoading = true;
    try {
      if (editedMaterialPage.value.id) {
        const res = await materialService.updateMaterials(
          editedMaterialPage.value.id,
          editedMaterialPage.value
        );
      } else {
        const res = await materialService.saveMaterials(
          editedMaterialPage.value
        );
        materials.value.push(editedMaterialPage.value);
      }

      dialog.value = false;
      await getMaterials();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Customer ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }
  function editMaterials(material: Material) {
    editedMaterialPage.value = JSON.parse(JSON.stringify(material));
    dialog.value = true;
  }

  async function deleteMaterial(id: number) {
    const confirmResult = await Swal.fire({
      title: "Are you sure?",
      text: "You will not be able to recover this customer!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
    });

    if (confirmResult.isConfirmed) {
      loadingStore.isLoading = true;
      try {
        const res = await materialService.deleteMaterials(id);
        await getMaterials();
        messageStore.showConfirm("ลบ Material สำเร็จ");
      } catch (e) {
        console.log(e);
        messageStore.showError("ไม่สามารถลบ Materail ได้");
      }
      loadingStore.isLoading = false;
    }
  }
  return {
    keyword,
    materials,
    getMaterials,
    lastPage,
    saveMaterial,
    editedMaterialPage,
    editMaterials,
    deleteMaterial,
  };
});
