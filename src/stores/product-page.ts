import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Customer from "@/types/Customer";
import customerService from "@/services/customer";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import Swal from "sweetalert2";
export const useCustomerStore = defineStore("Customer", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const keyword = ref("");
  const customers = ref<Customer[]>([]);
  const editedCustomer = ref<Customer>({
    name: "",
    tel: "",
    point: 0,
    start_date: "",
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedCustomer.value = { name: "", tel: "", point: 0, start_date: "" };
    }
  });
  async function getCustomers() {
    loadingStore.isLoading = true;
    try {
      const res = await customerService.getCustomers({
        keyword: keyword.value,
      });
      customers.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Customer ได้");
    }
    loadingStore.isLoading = false;
  }
  watch(page, async (newPage, oldPage) => {
    await getCustomers();
  });
  async function saveCustomer() {
    loadingStore.isLoading = true;
    try {
      if (editedCustomer.value.id) {
        const res = await customerService.updateCustomer(
          editedCustomer.value.id,
          editedCustomer.value
        );
      } else {
        const res = await customerService.saveCustomer(editedCustomer.value);
      }

      dialog.value = false;
      await getCustomers();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Customer ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteCustomer(id: number) {
    const confirmResult = await Swal.fire({
      title: "Are you sure?",
      text: "You will not be able to recover this customer!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
    });

    if (confirmResult.isConfirmed) {
      loadingStore.isLoading = true;
      try {
        const res = await customerService.deleteCustomer(id);
        await getCustomers();
        messageStore.showConfirm("ลบ Customer สำเร็จ");
      } catch (e) {
        console.log(e);
        messageStore.showError("ไม่สามารถลบ Customer ได้");
      }
      loadingStore.isLoading = false;
    }
  }
  function editCustomer(customer: Customer) {
    editedCustomer.value = JSON.parse(JSON.stringify(customer));
    dialog.value = true;
  }
  return {
    customers,
    getCustomers,
    dialog,
    editedCustomer,
    saveCustomer,
    editCustomer,
    deleteCustomer,
  };
});
