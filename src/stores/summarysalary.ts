import { defineStore } from "pinia";
import { useLoadingStore } from "./loading";
import summarysalaryService from "@/services/summarysalary";
// import { useMessageSuccessStore } from "./messageSuccess";
import { useMessageStore } from "./message";
import { ref } from "vue";
import type SummarySalary from "@/types/Summarysalary";
const loadingStore = useLoadingStore();
const messageStore = useMessageStore();
// const messageSuccessStore = useMessageSuccessStore();
const summarysalary = ref<SummarySalary[]>([]);

export const useSummarysalaryStore = defineStore("Summarysalary", () => {
  async function getSummarysalary() {
    loadingStore.isLoading = true;
    try {
      const res = await summarysalaryService.getSummarysalary();
      summarysalary.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("No Data Summarysalary");
    }
    loadingStore.isLoading = false;
  }
  return { summarysalary, getSummarysalary };
});
